import run from "aocrunner";

const Priority = Array.from('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ').reduce((acc, l, idx) => {
  acc[l] = idx + 1;
  return acc;
}, {});

const parseInput = (rawInput) => {
  return rawInput.split('\n')
};

const part1 = (rawInput) => {
  const bags = parseInput(rawInput);
  return bags.reduce((acc, bag) => {
    const compartmentA = bag.slice(0, bag.length / 2);
    const compartmentB = bag.slice(bag.length / 2);

    
    const { priority: bagItemsPriority } = Array.from(compartmentA).reduce((acc2, item) => {
      if (compartmentB.includes(item) && !acc2.items[item]) {
        acc2.items[item] = true;
        acc2.priority += Priority[item];
      }
      return acc2;
    }, { items: {}, priority: 0 });

    return acc + bagItemsPriority;
  }, 0);
};

const part2 = (rawInput) => {
  const bags = parseInput(rawInput);
  const groups = bags.reduce((acc, bag, idx) => {
    if (idx % 3 === 0) {
      acc.push([]);
    }

    acc[acc.length - 1].push(bag);

    return acc;
  }, []);

  return groups.reduce((acc, [compartmentA, compartmentB, compartmentC]) => {
    const { priority: bagItemsPriority } = Array.from(compartmentA).reduce((acc2, item) => {
      if (compartmentB.includes(item) && compartmentC.includes(item) && !acc2.items[item]) {
        acc2.items[item] = true;
        acc2.priority += Priority[item];
      }
      return acc2;
    }, { items: {}, priority: 0 });

    return acc + bagItemsPriority;
  }, 0);
};

run({
  part1: {
    tests: [
      {
        input: `
          vJrwpWtwJgWrhcsFMMfFFhFp
          jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
          PmmdzqPrVvPwwTWBwg
          wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
          ttgJtRGJQctTZtZT
          CrZsJsPPZsGzwwsLwLmpwMDw
        `,
        expected: 157,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          vJrwpWtwJgWrhcsFMMfFFhFp
          jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
          PmmdzqPrVvPwwTWBwg
          wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
          ttgJtRGJQctTZtZT
          CrZsJsPPZsGzwwsLwLmpwMDw
        `,
        expected: 70,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
