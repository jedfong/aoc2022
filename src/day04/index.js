import run from "aocrunner";

const parseInput = (rawInput) => {
  return rawInput.split('\n');
};

const part1 = (rawInput) => {
  return parseInput(rawInput).reduce((acc, input) => {
    const [a, b] = input.split(',');
    const [a1, a2] = a.split('-').map(a => parseInt(a, 10));
    const [b1, b2] = b.split('-').map(b => parseInt(b, 10));
    if ((a1 <= b1 && a2 >= b2) || (b1 <= a1 && b2 >= a2)) {
      acc++;
    }

    return acc;
  }, 0);
};

const part2 = (rawInput) => {
  return parseInput(rawInput).reduce((acc, input) => {
    const [a, b] = input.split(',');
    const [a1, a2] = a.split('-').map(a => parseInt(a, 10));
    const [b1, b2] = b.split('-').map(b => parseInt(b, 10));
    if (!(a2 < b1 || b2 < a1)) {
      acc++;
    }

    return acc;
  }, 0);
};

run({
  part1: {
    tests: [
      {
        input: `
          2-4,6-8
          2-3,4-5
          5-7,7-9
          2-8,3-7
          6-6,4-6
          2-6,4-8
        `,
        expected: 2,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          2-4,6-8
          2-3,4-5
          5-7,7-9
          2-8,3-7
          6-6,4-6
          2-6,4-8
        `,
        expected: 0,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
