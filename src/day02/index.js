import run from "aocrunner";

const Rock = 'Rock';
const Paper = 'Paper';
const Scissors = 'Scissors';
const Lose = 'Lose';
const Tie = 'Tie';
const Win = 'Win';

const Shapes = {
  A: Rock,
  X: Rock,
  B: Paper,
  Y: Paper,
  C: Scissors,
  Z: Scissors,
};

const Conditions = {
  X: Lose,
  Y: Tie,
  Z: Win,
}

const parseInput1 = (rawInput) => {
  return rawInput.split('\n').map(input => {
    const [rawElfShape, rawYouShape] = input.split(' ');
    return [
      Shapes[rawElfShape],
      Shapes[rawYouShape],
    ];
  });
};

const parseInput2 = (rawInput) => {
  return rawInput.split('\n').map(input => {
    const [rawElfShape, rawCondition] = input.split(' ');
    return [
      Shapes[rawElfShape],
      Conditions[rawCondition],
    ];
  });
};

const computeShapeScore = (shape) => {
  switch (shape) {
    case 'Rock':
      return 1;
    case 'Paper':
      return 2;
    case 'Scissors':
      return 3;
  }
}

const computeWinCondition = (elfShape, youShape) => {
  if (elfShape === youShape) {
    return Tie;
  }
  
  if (elfShape === Rock) {
    return youShape === Paper ? Win : Lose;
  }

  if (elfShape === Paper) {
    return youShape === Scissors ? Win : Lose;
  }

  if (elfShape === Scissors) {
    return youShape === Rock ? Win : Lose;
  }
};

const computeWinScore = (condition) => {
  switch (condition) {
    case Win:
      return 6;
    case Tie:
      return 3;
    case Lose:
    default:
      return 0;
  }
};

const computeYouShape = (elfShape, condition) => {
  if (condition === Tie) {
    return elfShape;
  }

  if (elfShape === Rock) {
    return condition === Win ? Paper : Scissors;
  }

  if (elfShape === Paper) {
    return condition === Win ? Scissors : Rock;
  }

  if (elfShape === Scissors) {
    return condition === Win ? Rock : Paper;
  }
};

const part1 = (rawInput) => {
  const input = parseInput1(rawInput);
  return input.reduce((acc, [elfShape, youShape]) => {
    const condition = computeWinCondition(elfShape, youShape);
    // console.log(`I played ${youShape} (${computeShapeScore(youShape)}) and the elf played ${elfShape}; Condition is ${condition} (${computeWinScore(condition)})`)
    return acc + computeShapeScore(youShape) + computeWinScore(condition);
  }, 0);
};

const part2 = (rawInput) => {
  const input = parseInput2(rawInput);
  return input.reduce((acc, [elfShape, condition]) => {
    const youShape = computeYouShape(elfShape, condition);
    // console.log(`Elf played ${elfShape} and the condition is ${condition} (${computeWinScore(condition)}); I should play ${youShape} (${computeShapeScore(youShape)});`)
    return acc + computeShapeScore(youShape) + computeWinScore(condition);
  }, 0);
};

run({
  part1: {
    tests: [
      {
        input: `
          A Y
          B X
          C Z
        `,
        expected: 15,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          A Y
          B X
          C Z
        `,
        expected: 12,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
